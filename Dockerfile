FROM nginx:alpine

COPY conf/default.conf /etc/nginx/conf.d/
COPY conf/index.html /usr/share/nginx/html
